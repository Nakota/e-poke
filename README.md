# Installation

```
npm install
```

```
npm run start
```

# Présentation

Site e-commerce pour que Pierre puisse acheter des cartes pokémons pour compléter sa collection.

Utilisation de :

- Ngxs pour gérer le store de son panier
- Angular Material pour le côté design sympathique et rapide

# Améliorations à apporter

- Garder dans l'url la recherche sur la liste des cartes
- Ajouter un interceptor pour prendre en compte une API key de pokemon TCG API
- Compléter la vue d'une carte pour avoir plus d'informations (évolutions du prix, type pokémon, etc)
