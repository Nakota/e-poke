import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Card } from '../models/tcg/card';
import { BasketState } from '../store/basket.state';

@Pipe({
  name: 'quantityCardInBasket$',
})
export class QuantityCardInBasketPipe implements PipeTransform {
  constructor(private store: Store) {}

  transform(card: Card): Observable<number> {
    return this.store.select(BasketState.quantityByIdCard(card.id));
  }
}
