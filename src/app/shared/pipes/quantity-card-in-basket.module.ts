import { NgModule } from '@angular/core';
import { QuantityCardInBasketPipe } from './quantity-card-in-basket.pipe';

@NgModule({
  declarations: [QuantityCardInBasketPipe],
  exports: [QuantityCardInBasketPipe],
})
export class QuantityCardInBasketModule {}
