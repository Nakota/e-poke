import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Card } from '../models/tcg/card';
import { Pagination } from '../models/pagination';
import { TcgFilter } from '../models/tcg/filter';
import { TcgPaginationResult } from '../models/tcg/paginationResult';

@Injectable({
  providedIn: 'root',
})
export class PokemonsService {
  constructor(private http: HttpClient) {}

  getAll$(pagination: Pagination, filters?: TcgFilter[]): Observable<TcgPaginationResult<Card>> {
    let url = `${environment.pokemonApi.url}/cards?page=${pagination.page}&pageSize=${pagination.limit}&orderBy=name,-number`;
    filters = filters?.filter(filter => !!filter.value);
    if (!!filters?.length) {
      url += `&q=${filters.map(filter => this._addQueryParam(filter.name, filter.value)).join(' ')}`;
    }
    return this.http.get<TcgPaginationResult<Card>>(url);
  }

  get$(id: string): Observable<Card> {
    return this.http.get(`${environment.pokemonApi.url}/cards/${id}`).pipe(map((res: any) => res.data));
  }

  getRarities$(): Observable<string[]> {
    return this.http.get(`${environment.pokemonApi.url}/rarities`).pipe(map((res: any) => res.data));
  }

  private _addQueryParam(name: string, value: number | string | string[]): string | null {
    if (!!value) {
      if (Array.isArray(value)) {
        if (value.length > 0) {
          return `(${value.map(v => `${name}:"${v}"`).join(' OR ')})`;
        }
        return null;
      }
      return `${name}:${value}`;
    }
    return null;
  }
}
