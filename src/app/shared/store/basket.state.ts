import { Injectable } from '@angular/core';
import { Action, createSelector, Select, Selector, State, StateContext } from '@ngxs/store';
import { insertItem, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { Observable } from 'rxjs';
import { BasketCard } from '../models/basketCard';
import { Card } from '../models/tcg/card';

export class BasketStateModel {
  cards: BasketCard[];
}

export class BasketAddCard {
  static readonly type = '[Basket] Increment card';
  constructor(public card: Card) {}
}

export class BasketRemoveCard {
  static readonly type = '[Basket] Decrement card';
  constructor(public id: string) {}
}

export class BasketRemoveAllCard {
  static readonly type = '[Basket] Remove all same card';
  constructor(public id: string) {}
}

export class BasketReset {
  static readonly type = '[Basket] Reset basket';
}

@State<BasketStateModel>({
  name: 'basket',
  defaults: {
    cards: [],
  },
})
@Injectable()
export class BasketState {
  @Selector()
  static cards(state: BasketStateModel): BasketCard[] {
    return state.cards;
  }

  @Selector()
  static price(state: BasketStateModel): number {
    return state.cards.reduce((value, card) => value + card.quantity * card.cardmarket.prices.trendPrice, 0);
  }

  @Selector()
  static quantity(state: BasketStateModel): number {
    return state.cards.reduce((value, card) => value + card.quantity, 0);
  }

  static quantityByIdCard(idCard: string): (state: BasketStateModel) => number {
    return createSelector([BasketState], (state: BasketStateModel): number => {
      return state.cards.filter(card => card.id === idCard).reduce((value, card) => value + card.quantity, 0);
    });
  }

  constructor() {}

  @Action(BasketAddCard)
  incrementCard(ctx: StateContext<BasketStateModel>, { card }: BasketAddCard): void {
    const cardInBasket = ctx.getState().cards.find(c => c.id === card.id);
    if (cardInBasket) {
      // Card already in basket - increment quantity
      ctx.setState(
        patch({
          cards: updateItem<BasketCard>(c => c.id === card.id, {
            ...cardInBasket,
            quantity: cardInBasket.quantity + 1,
          }),
        }),
      );
    } else {
      // Card not already added in basket
      ctx.setState(
        patch({
          cards: insertItem({ ...card, quantity: 1 }),
        }),
      );
    }
  }

  @Action(BasketRemoveCard)
  decrementCard(ctx: StateContext<BasketStateModel>, { id }: BasketRemoveCard): void {
    const cardInBasket = ctx.getState().cards.find(c => c.id === id);
    if (cardInBasket && cardInBasket.quantity > 1) {
      // Card already in basket - decrement quantity
      ctx.setState(
        patch({
          cards: updateItem<BasketCard>(c => c.id === id, {
            ...cardInBasket,
            quantity: cardInBasket.quantity - 1,
          }),
        }),
      );
    } else {
      // Card not already added in basket
      ctx.setState(
        patch({
          cards: removeItem((c: BasketCard) => c.id === id),
        }),
      );
    }
  }

  @Action(BasketRemoveAllCard)
  removeCard(ctx: StateContext<BasketStateModel>, { id }: BasketRemoveCard): void {
    ctx.setState(
      patch({
        cards: removeItem((c: BasketCard) => c.id === id),
      }),
    );
  }

  @Action(BasketReset)
  reset({ patchState }: StateContext<BasketStateModel>, { id }: BasketRemoveCard): void {
    patchState({
      cards: [],
    });
  }
}
