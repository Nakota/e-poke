import { Card } from './tcg/card';

export interface BasketCard extends Card {
  quantity: number;
}
