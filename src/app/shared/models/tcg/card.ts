export interface Card {
  id: string;
  name: string;
  images: {
    small: string;
    large: string;
  };
  cardmarket: {
    prices: {
      trendPrice: number | null;
    };
  };
  artist?: string;
  rarity: string;
}
