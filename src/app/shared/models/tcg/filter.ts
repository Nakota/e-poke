export interface TcgFilter {
  name: string;
  value: string | number | string[];
}
