import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasketListingComponent } from './basket-listing.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [BasketListingComponent],
  imports: [CommonModule, MatButtonModule, MatIconModule, MatProgressSpinnerModule],
  exports: [BasketListingComponent],
})
export class BasketListingModule {}
