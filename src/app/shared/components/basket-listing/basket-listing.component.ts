import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { of } from 'rxjs';
import { delay, switchMap, take, tap } from 'rxjs/operators';
import { BasketRemoveAllCard, BasketReset, BasketState } from '../../store/basket.state';

@Component({
  selector: 'epk-basket-listing',
  templateUrl: './basket-listing.component.html',
  styleUrls: ['./basket-listing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasketListingComponent {
  @Output() paymentProcessing: EventEmitter<void> = new EventEmitter();
  @Output() paymentEnded: EventEmitter<void> = new EventEmitter();

  cards$ = this.store.select(BasketState.cards);
  price$ = this.store.select(BasketState.price);
  isPaying = false;

  constructor(private store: Store) {}

  remove(id: string): void {
    this.store.dispatch(new BasketRemoveAllCard(id));
  }

  fakePay(): void {
    this.isPaying = true;
    this.paymentProcessing.emit();
    of(1)
      .pipe(
        take(1),
        delay(3000),
        tap(() => this.paymentEnded.emit()),
        switchMap(() => this.store.dispatch(new BasketReset())),
        tap(() => (this.isPaying = false)),
      )
      .subscribe();
  }
}
