import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketListingComponent } from './basket-listing.component';

describe('BasketListingComponent', () => {
  let component: BasketListingComponent;
  let fixture: ComponentFixture<BasketListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasketListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
