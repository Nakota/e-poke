import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { QuantityCardInBasketModule } from '../../pipes/quantity-card-in-basket.module';
import { CardSelectComponent } from './card-select.component';

@NgModule({
  declarations: [CardSelectComponent],
  imports: [CommonModule, RouterModule, MatIconModule, MatButtonModule, QuantityCardInBasketModule],
  exports: [CardSelectComponent],
})
export class CardSelectModule {}
