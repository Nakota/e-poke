import { ChangeDetectionStrategy, Component, HostListener, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { Card } from '../../models/tcg/card';
import { BasketAddCard, BasketRemoveCard } from '../../store/basket.state';

@Component({
  selector: 'epk-card-select',
  templateUrl: './card-select.component.html',
  styleUrls: ['./card-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardSelectComponent {
  @Input() card: Card;

  isHover = false;

  constructor(private store: Store) {}

  @HostListener('mouseover')
  onHover() {
    this.isHover = true;
  }

  @HostListener('mouseout')
  onMouseOut() {
    this.isHover = false;
  }

  add(): void {
    this.store.dispatch(new BasketAddCard(this.card));
  }

  remove(): void {
    this.store.dispatch(new BasketRemoveCard(this.card.id));
  }
}
