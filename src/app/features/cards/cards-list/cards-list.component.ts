import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { BehaviorSubject, merge, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Card } from 'src/app/shared/models/tcg/card';
import { TcgPaginationResult } from 'src/app/shared/models/tcg/paginationResult';
import { PokemonsService } from 'src/app/shared/services/pokemons.service';

@Component({
  selector: 'epk-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardsListComponent implements OnInit, OnDestroy {
  isLoading = true;
  destroy$ = new Subject();

  // Paginator
  length = 10;
  pageSize = 10;
  pageSizeOptions = [10];

  form = this.fb.group({
    name: [''],
    rarities: [[]],
    page: [1],
  });
  raritiesList$ = this.pokemonsService.getRarities$();
  reloadSearch$ = new BehaviorSubject({
    ...this.form.value,
  });
  cards$ = this.reloadSearch$.pipe(
    debounceTime(400),
    distinctUntilChanged(),
    switchMap(filters =>
      this.pokemonsService.getAll$(
        {
          page: filters.page,
          limit: this.pageSize,
        },
        [
          { name: 'name', value: filters.name },
          { name: 'rarity', value: filters.rarities },
        ],
      ),
    ),
    tap(res => this._setPaginatorDatas(res)),
    map(res => res.data),
    tap(() => (this.isLoading = false)),
    finalize(() => this.cd.markForCheck()),
  );

  constructor(private pokemonsService: PokemonsService, private fb: FormBuilder, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    merge(this.form.get('name').valueChanges, this.form.get('rarities').valueChanges)
      .pipe(
        takeUntil(this.destroy$),
        tap(() => this.form.get('page').patchValue(1)),
      )
      .subscribe();

    this.form.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        tap(values =>
          this.reloadSearch$.next({
            ...values,
          }),
        ),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  selectPage(event: PageEvent): void {
    this.form.get('page').patchValue(event.pageIndex + 1);
  }

  trackByCard(index: number, card: Card) {
    return card.id;
  }

  private _setPaginatorDatas(result: TcgPaginationResult<Card>): void {
    this.length = result.totalCount;
  }
}
