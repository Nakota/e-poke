import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsListModule } from './cards-list/cards-list.module';
import { CardsRoutingModule } from './cards-routing.module';
import { CardModule } from './card/card.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CardsRoutingModule, CardsListModule, CardModule],
})
export class CardsModule {}
