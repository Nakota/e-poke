import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Card } from 'src/app/shared/models/tcg/card';
import { PokemonsService } from 'src/app/shared/services/pokemons.service';
import { BasketAddCard, BasketRemoveCard, BasketState } from 'src/app/shared/store/basket.state';

@Component({
  selector: 'epk-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {
  card$ = this.pokemonsService.get$(this.route.snapshot.params.id);
  quantity$: Observable<number>;

  constructor(private route: ActivatedRoute, private pokemonsService: PokemonsService, private store: Store) {}

  add(card: Card): void {
    this.store.dispatch(new BasketAddCard(card));
  }

  remove(id: string): void {
    this.store.dispatch(new BasketRemoveCard(id));
  }
}
