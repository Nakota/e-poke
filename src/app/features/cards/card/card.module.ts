import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { QuantityCardInBasketModule } from 'src/app/shared/pipes/quantity-card-in-basket.module';
import { CardComponent } from './card.component';

@NgModule({
  declarations: [CardComponent],
  imports: [CommonModule, RouterModule, MatIconModule, MatButtonModule, MatIconModule, QuantityCardInBasketModule],
})
export class CardModule {}
