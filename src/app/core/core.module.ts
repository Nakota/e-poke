import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreRoutingModule } from './core-routing.module';
import { LoggedModule } from './layouts/logged/logged.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CoreRoutingModule, LoggedModule],
})
export class CoreModule {}
