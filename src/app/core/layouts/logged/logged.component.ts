import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Store } from '@ngxs/store';
import { BasketState } from 'src/app/shared/store/basket.state';

@Component({
  selector: 'epk-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['./logged.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoggedComponent {
  price$ = this.store.select(BasketState.price);
  quantity$ = this.store.select(BasketState.quantity);

  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(private store: Store) {}

  toggleSidenav(): void {
    if (!this.sidenav.disableClose) {
      this.sidenav.toggle();
    }
  }

  lockCloseSidenav(): void {
    this.sidenav.disableClose = true;
  }

  paymentEnded(): void {
    this.sidenav.disableClose = false;
    this.sidenav.close();
  }
}
